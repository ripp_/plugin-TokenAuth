require_relative "SimplePlugin"

class SimplePlugin < Plugin

	class << self
		attr_accessor :gets
		attr_accessor :posts
	end

	@gets = nil
	@posts = nil

	def startup
		safeApiBase = "/" + self.class.name[0].downcase + self.class.name[1..-1]


		if self.class.posts.nil?
			postMethodNames = []
		else
			postMethodNames = self.class.posts
		end

		if self.class.gets.nil?
			getMethodNames = self.class.public_instance_methods(false)
		else
			getMethodNames = self.class.get
		end
		getMethodNames -= postMethodNames

		getEndpoints = getMethodNames.collect {|x| safeApiBase +"/"+x.to_s }
		postEndpoints = postMethodNames.collect {|x| safeApiBase +"/"+x.to_s }

		getMethods = getMethodNames.collect {|x| method(x)}
		@getMapping = Hash[getEndpoints.zip(getMethods)]

		postMethods = postMethodNames.collect {|x| method(x)}
		@postMapping = Hash[postEndpoints.zip(postMethods)]

		config = {
			:status => "success",
			:endpoints => {
				:get => getEndpoints,
				:post => postEndpoints
			}
		}

		return JSON.generate(extraStartup(config))
	end

	private def extraStartup(config)
		return config
	end

	def request(endpoint,getData)
		if @getMapping.key? endpoint
			method = @getMapping[endpoint]
		else
			method = @postMapping[endpoint]
		end
		parameters = method.parameters
		getData = JSON.parse(getData)
		poped = []
		parameters.each do |type,name|
			if getData.key? name.to_s
				poped << getData[name.to_s]
			else
				if type == :req
					return {:success => false,:error => "Missing paramater: "+name.to_s}
				end
			end
		end
		return method.call(*poped)
	end
end
