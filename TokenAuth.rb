require "net-ldap"
require "digest"


class TokenAuth < SimplePlugin


  @posts = [
    :authenticate
  ]

  private def extraStartup(config)
    @ldap = Net::LDAP.new
    @ldap.host = "silver.sucs.swan.ac.uk"
    @ldap.port = 389

    @storedInfo = {}
    @authedTokens = {}

    @sign_token_timeout = 60*5
    @auth_token_timeout = 60*60*24

    return config
  end

  def auth_login_page(token)
    if @storedInfo.has_key?(token)
      file = File.open("./plugins/TokenAuth/auth_login_page.html", "r")
      data = file.read
      file.close
      return data.sub! '{{token}}',token
    else
      return "Bad Token"
    end
  end

  def request_token(customer_id,callback)
    token = Digest::MD5.hexdigest(rand.to_s)
    secret = Digest::MD5.hexdigest(rand.to_s)
    @storedInfo[token] = {
      :customerId => customer_id,
      :expires => Time.new + @sign_token_timeout,
      :callback => callback,
      :authed => 0
    }
    return {:token=>token}
  end

  def authenticate(token,username,password)
    puts token
    if not @storedInfo.has_key?(token)
      return {:success => false,:error => "Bad token"}
    elsif @storedInfo[token][:authed] != 0
      return {:success => false,:error => "token already used"}
    elsif @storedInfo[token][:expires] < Time.new
      @storedInfo.delete(token)
      return {:success => false,:error => "token has expired"}
    #Ok everything looks good
    else
      if true or @ldap.bind(
				:method => :simple,
				:username => "uid="+username+",ou=People,dc=sucs,dc=org",
				:password => password
			)
        @storedInfo[token][:expires] = Time.new + @sign_token_timeout
        @storedInfo[token][:authed] = 1
        @storedInfo[token][:user] = username
        puts 
        puts '<meta http-equiv="refresh" content="0; url='+@storedInfo[token][:callback]+'?token='+token+'"/>'
        return '<meta http-equiv="refresh" content="0; url='+@storedInfo[token][:callback]+'?token='+token+'"/>'
  			#return {:success => true,:redirect => "",:token => token }
  		else
        #(It stopped looking good)
        @storedInfo.delete(token)
  			return {:success => false,:error => "Bad login"}
      end
		end
	end

  def access_token(token)
    puts @storedInfo

    if not @storedInfo.has_key?(token)
      return {:success => false,:error => "Bad token"}
    elsif @storedInfo[token][:expires] < Time.new
      @storedInfo.delete(token)
      return {:success => false,:error => "token has expired"}
    else
      #We are good to go, generate this boy a auth token for requests!
      begin
        authToken = Digest::MD5.hexdigest(rand.to_s)
      end while @authedTokens.has_key?(authToken)
      @authedTokens[authToken] = {
        :user => @storedInfo[token][:user],
        :customerId => @storedInfo[token][:customerId],
        :expires => Time.new + @auth_token_timeout
      }
      @storedInfo.delete(token)
      return {
        :success => true, :authToken=>authToken
      }
    end
  end


  def auth_token_good(authToken)
    if @authedTokens.has_key?(authToken)
      return {:success => true,:user => @authedTokens[authToken][:user]}
    else
      return {:success => false, :error=>"Bad Auth token"}
    end
  end

end
